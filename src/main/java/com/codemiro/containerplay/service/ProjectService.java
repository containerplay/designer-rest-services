package com.codemiro.containerplay.service;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.codemiro.containerplay.repository.ProjectRepository;
import com.codemiro.containerplay.repository.entity.NewApp;
import com.codemiro.containerplay.repository.entity.Project;
import com.codemiro.containerplay.web.request.ProjectRequest;
import com.querydsl.core.types.Predicate;

/**
 * @author dvsingh9
 */

@Service
public class ProjectService implements BaseService<Project, ProjectRequest> {

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Page<Project> getAll(Predicate predicate, Pageable pageable) {
		return projectRepository.findAll(predicate, pageable);
	}
	
	@Override
	public Page<Project> getAll(String search, Pageable pageable) {
		return projectRepository.findAll(pageable);
	}

	@Override
	public List<Project> getAll(String search) {
		return projectRepository.findAll();
	}

	@Override
	public Project getById(Long id) {
		Project project = projectRepository.getOne(id);
		if(project.getName() == null) {
			throw new NoSuchElementException("Project not found to update #" + id);
		}
		return project;
	}

	@Override
	@Transactional
	public Project create(ProjectRequest request) {
		Project project = Project.builder().id(request.getProjectId()).name(request.getName())
				.description(request.getDescription()).projectHTML(request.getProjectHTML())
				.projectJsonConfigs(request.getProjectJsonConfigs()).build();
		return projectRepository.save(project);
	}

	@Override
	@Transactional
	public void update(Long id, ProjectRequest request) {
		Project project = projectRepository.getOne(id);
		if (project.getName() == null) {
			throw new NoSuchElementException("Project not found to update #" + id);
		}
		project.setName(request.getName());
		project.setDescription(request.getDescription());
		project.setProjectHTML(request.getProjectHTML());
		project.setProjectJsonConfigs(request.getProjectJsonConfigs());
		projectRepository.save(project);
	}

	@Override
	public void delete(Long id) {
		Project project = projectRepository.getOne(id);
		if (project != null) {
			projectRepository.deleteById(id);
		} else {
			throw new NoSuchElementException("No Project Found to delete #" + id);
		}

	}

	

}
