package com.codemiro.containerplay.web;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;

import com.querydsl.core.types.Predicate;

import java.util.List;

/**
 * @author dvsingh9
 */

public interface BaseController<I, O> {
	Page<O> getAll(Predicate predicate, Pageable pageable);
    Page<O> getAll(String search, Pageable pageable);
    List<O> getAll(String search);
    O getById(Long id);
    O create(I request, BindingResult result);
    void update(Long id, I request, BindingResult result);
    void delete(Long id);
}
