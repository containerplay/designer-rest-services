package com.codemiro.containerplay.repository.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dvsingh
 * @category persistence
 *
 */

//Documentation for swagger
@ApiModel(description="All details about the project.")
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Project extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2267303830593818701L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;

	private String name;
	private String description;
	private String remarks;
	// @Lob
	private String projectHTML;
	// @Lob
	private String projectJsonConfigs;

}
