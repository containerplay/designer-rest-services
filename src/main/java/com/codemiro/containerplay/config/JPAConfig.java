package com.codemiro.containerplay.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration 
@EnableJpaRepositories("com.codemiro.containerplay.repository")
public class JPAConfig {
	
}
