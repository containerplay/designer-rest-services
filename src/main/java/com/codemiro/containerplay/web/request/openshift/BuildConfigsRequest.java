package com.codemiro.containerplay.web.request.openshift;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.fabric8.kubernetes.api.model.Initializers;
import io.fabric8.kubernetes.api.model.OwnerReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BuildConfigsRequest implements Serializable {
	private static final long serialVersionUID = -6520021527105319305L;

    @Valid
    private Map<String, String> annotations;
    
    private java.lang.String clusterName;
    
    @Valid
    private String creationTimestamp;
    
    private Long deletionGracePeriodSeconds;
    
    @Valid
    private String deletionTimestamp;
    
    @Valid
    private List<java.lang.String> finalizers = new ArrayList<java.lang.String>();
    
    private java.lang.String generateName;
    
    private Long generation;
    
    @Valid
    private Initializers initializers;
    
    @Valid
    private Map<String, String> labels;
    
    private java.lang.String name;
    
    @Pattern(regexp = "^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$")
    @Size(max = 253)
    private java.lang.String namespace;
   
    @Valid
    private List<OwnerReference> ownerReferences = new ArrayList<OwnerReference>();
   
    private java.lang.String resourceVersion;
    
    private java.lang.String selfLink;
    
    private java.lang.String uid;
    @JsonIgnore
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();


}
