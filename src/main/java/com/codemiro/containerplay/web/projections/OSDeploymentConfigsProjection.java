package com.codemiro.containerplay.web.projections;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

public interface OSDeploymentConfigsProjection {

	 String getApiVersion();
	 
	 @Value("#{target.getMetadata() != null ? target.getMetadata().getName() : null}")
	 String getName();
	 
	 String getKind();
	 
	 @Value("#{target.getSpec() != null ? target.getSpec().getStrategy().getType() : null}")
	 String getSpecType();
	 
	 @Value("#{target.getSpec() != null ? target.getSpec().getTriggers() : null}")
	 List getSpecTriggers();
	 
	 @Value("#{target.getStatus() !=null ? target.getStatus().getReadyReplicas() : null}")
	 Integer getReplicas();
	 
	 @Value("#{target.getStatus() !=null ? target.getStatus().getAvailableReplicas() : null}")
	 Integer getMyAvailableReplicas();
}
