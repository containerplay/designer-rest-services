package com.codemiro.containerplay.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.fabric8.kubernetes.client.AutoAdaptableKubernetesClient;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.openshift.client.DefaultOpenShiftClient;
import io.fabric8.openshift.client.OpenShiftClient;

@Configuration
public class OpenShiftConfig {

	@Value("${openshift.conn.url}")
	String connectionURL;

	@Value("${openshift.username}")
	String username;

	@Value("${openshift.password}")
	String password;

//	@Bean
//	public KubernetesClient getClusterConnection() {
//		// Config config = new
//		// ConfigBuilder().withMasterUrl(connectionURL).withTrustCerts(true).withUsername(username)
//		// .withPassword(password).withNamespace("dev-env").build(); // getting
//		// connection for dev-env
//		Config config = new ConfigBuilder().withMasterUrl(connectionURL).withTrustCerts(true).withUsername(username)
//				.withPassword(password).build(); // getting connection for
//													// dev-env
//		// return new AutoAdaptableKubernetesClient(config);
//		return new AutoAdaptableKubernetesClient(config);
//	}

	@Bean
	public OpenShiftClient openShiftClient() {
		return new DefaultOpenShiftClient();
	}

}
