package com.codemiro.containerplay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.stereotype.Repository;

import com.codemiro.containerplay.repository.entity.Project;
import com.codemiro.containerplay.repository.entity.QProject;
import com.querydsl.core.types.dsl.StringPath;

@Repository
public interface ProjectRepository
		extends JpaRepository<Project, Long>, QuerydslPredicateExecutor<Project>, QuerydslBinderCustomizer<QProject> {

	@Override
	default void customize(QuerydslBindings bindings, QProject qProject) {
		bindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
	}
}
