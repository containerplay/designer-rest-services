package com.codemiro.containerplay.service.openshift;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codemiro.containerplay.web.request.openshift.BuildConfigsRequest;

import io.fabric8.openshift.api.model.BuildConfig;
import io.fabric8.openshift.client.DefaultOpenShiftClient;
import io.fabric8.openshift.client.OpenShiftClient;
import io.fabric8.openshift.client.OpenShiftNotAvailableException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BuildConfigsService {

	private final OpenShiftClient openShiftClient;
	
	@Autowired
	public BuildConfigsService(OpenShiftClient openShiftClient) {
		this.openShiftClient = new DefaultOpenShiftClient();
	}

	public List<BuildConfig> getAll(String namespace) {
		// OpenShiftClient client = new DefaultOpenShiftClient();
		if (namespace == null) {
			log.info("Finding all BuildConfigs");
			return openShiftClient.buildConfigs().list().getItems();
		} else {
			log.info("Finding all BuildConfigs for namespace#" + namespace);
			return openShiftClient.buildConfigs().inNamespace(namespace).list().getItems();
		}
	}

	public BuildConfig create(BuildConfig request) {
		try {
			request = openShiftClient.buildConfigs().createOrReplace(request);
			log.info("BuildConfig updated for request #"+ request);
			return request;
		} catch (Exception e) {
			log.error("Unable to add/update BuildConfig for request #"+ request);
			throw new OpenShiftNotAvailableException("Unable to add/update BuildConfig");
		}
	}

	public void update(Long id, BuildConfigsRequest request) {

	}

	public boolean delete(BuildConfig request) {
		return openShiftClient.buildConfigs().delete(request);
	}

}
