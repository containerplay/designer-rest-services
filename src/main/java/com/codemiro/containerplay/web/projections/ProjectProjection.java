package com.codemiro.containerplay.web.projections;


/**
 * @author dvsingh9
 */

public interface ProjectProjection {
	Long getId();
	String getName();
	String getDescription();
	String getRemarks();
	String getProjectHTML();
	String getProjectJsonConfigs();
}
