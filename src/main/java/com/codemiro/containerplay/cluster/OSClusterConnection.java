package com.codemiro.containerplay.cluster;

import io.fabric8.kubernetes.client.AutoAdaptableKubernetesClient;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;

/**
 * @author anuragsaran
 *
 */
public class OSClusterConnection {

	private  KubernetesClient client;
	public OSClusterConnection() {
		super();
	}

	public KubernetesClient getClusterConnection() {
    String master = "https://oc-cluster.anuragsdemo.club:8443/";
    Config config = new ConfigBuilder().withMasterUrl(master)
      .withTrustCerts(true)
      .withUsername("developer")
      .withPassword("developer")
      .withNamespace("dev-env")
      .build();
		
    return client = new AutoAdaptableKubernetesClient(config);
    	 	
	}
}
