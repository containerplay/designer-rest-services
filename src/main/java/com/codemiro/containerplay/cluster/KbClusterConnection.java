package com.codemiro.containerplay.cluster;

import io.fabric8.openshift.client.DefaultOpenShiftClient;
import io.fabric8.openshift.client.OpenShiftClient;

/**
 * @author anuragsaran
 *
 */
public class KbClusterConnection {

	private   OpenShiftClient client;
	public KbClusterConnection() {
		super();
	}

	public OpenShiftClient getClusterConnection() {
   OpenShiftClient client = new DefaultOpenShiftClient();
		
    return client;
    	 	
	}
}
