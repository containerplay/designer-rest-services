package com.codemiro.containerplay.common;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class LoggingAspect {

	public LoggingAspect() {}
	
	@Before("execution(* com.codemiro.containerplay..*.*(..))")
	public void logMethodAccessBefore(JoinPoint joinPoint) {
		log.info("----Starting: " +joinPoint.getSignature().getClass().getSimpleName() + " > "+  joinPoint.getSignature().getName() + "() ----");
	}

	@AfterReturning("execution(* com.codemiro.containerplay..*.*(..))")
	public void logMethodAccessAfter(JoinPoint joinPoint) {
		log.info("----Completed: " +joinPoint.getSignature().getClass().getSimpleName() + " > "+  joinPoint.getSignature().getName() + "() ----");
	}

	
}
