package com.codemiro.containerplay.examples;

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import java.util.HashMap;
import java.util.Map;

import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

/**
 * This example shows how to use {@link SpelAwareProxyProjectionFactory} to create simple projection proxies backed by a
 * model and backed by a map. Proxies with SpEL support require further configuration of
 * {@link SpelAwareProxyProjectionFactory}.
 * 
 * @author Mark Paluch
 */
public class StandaloneProjectionExample {

	public static void main(String[] args) {

		MyModel mymodel = new MyModel();
		mymodel.setName("Walter");

		ProjectionFactory pf = new SpelAwareProxyProjectionFactory();
		Projection backedByBean = pf.createProjection(Projection.class, mymodel);

		System.out.println(backedByBean.getName());

		Map<String, Object> map = new HashMap<>();
		map.put("name", "White");

		Projection backedByMap = pf.createProjection(Projection.class, map);
		System.out.println(backedByMap.getName());
	}

	static interface Projection {

		String getName();
	}

	public static class MyModel {

		private Long modelId;
		private long version;
		private String name;

		public Long getModelId() {
			return modelId;
		}

		public void setModelId(Long modelId) {
			this.modelId = modelId;
		}

		public long getVersion() {
			return version;
		}

		public void setVersion(long version) {
			this.version = version;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
}