package com.codemiro.containerplay.repository.entity;

import javax.persistence.Lob;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NewApp {

	private String gitUrl;
	private String name;
	private String imageName;
	private String tempateName;
	private String tempateParametes;
	private String environmentParametes;
	private String labelList;
}
