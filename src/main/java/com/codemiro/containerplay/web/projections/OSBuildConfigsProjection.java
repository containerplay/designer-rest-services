package com.codemiro.containerplay.web.projections;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

public interface OSBuildConfigsProjection {

	 String getApiVersion();
	 
	 @Value("#{target.getMetadata() != null ? target.getMetadata().getName() : null}")
	 String getName1();
	 
	 String getKind();
	 
	 @Value("#{target.getSpec() != null ? target.getSpec().getStrategy().getType() : null}")
	 String getSpecType();
	 
	 @Value("#{target.getSpec() != null ? target.getSpec().getOutput() : null}")
	 List getSpecTriggers();
}
