package com.codemiro.containerplay.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codemiro.containerplay.repository.entity.Project;
import com.codemiro.containerplay.service.ProjectService;
import com.codemiro.containerplay.web.exception.ValidationException;
import com.codemiro.containerplay.web.projections.ProjectProjection;
import com.codemiro.containerplay.web.request.ProjectRequest;
import com.querydsl.core.types.Predicate;

/**
 * @author dvsingh9
 */

@RestController
@RequestMapping(value = "/v1/projects")
public class ProjectController implements BaseController<ProjectRequest, ProjectProjection> {

	@Autowired
	private ProjectService projectService;
	@Autowired
	private ProjectionFactory projectionFactory;

	@Override
	@GetMapping("/all")
	public Page<ProjectProjection> getAll(@QuerydslPredicate(root = Project.class) Predicate predicate, Pageable pageable) {
		return projectService.getAll(predicate, pageable)
				.map(x -> projectionFactory.createProjection(ProjectProjection.class, x));
	}

	@Override
	@GetMapping("/search/page")
	public Page<ProjectProjection> getAll(String search, Pageable pageable) {
		return projectService.getAll(search, pageable)
				.map(x -> projectionFactory.createProjection(ProjectProjection.class, x));
	}

	@Override
	@GetMapping("/search/all")
	public List<ProjectProjection> getAll(String search) {
		return projectService.getAll(search).stream()
				.map(x -> projectionFactory.createProjection(ProjectProjection.class, x)).collect(Collectors.toList());
	}

	@Override
	@GetMapping("/{id}")
	public ProjectProjection getById(@PathVariable("id") Long id) {
		return projectionFactory.createProjection(ProjectProjection.class, projectService.getById(id));
	}

	@Override
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ProjectProjection create(@RequestBody @Valid ProjectRequest request, BindingResult result) {
		if (result.hasErrors())
			throw new ValidationException("Project Add#", result.getFieldErrors());
		return projectionFactory.createProjection(ProjectProjection.class, projectService.create(request));
	}

	@Override
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void update(@PathVariable("id") Long id, @Valid ProjectRequest request, BindingResult result) {
		if (result.hasErrors())
			throw new ValidationException("Project Edit#", result.getFieldErrors());
		projectService.update(id, request);
	}

	@Override
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {
		projectService.delete(id);
	}

}
