package com.codemiro.containerplay.web.openshift;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codemiro.containerplay.service.openshift.BuildConfigsService;
import com.codemiro.containerplay.web.exception.ValidationException;
import com.codemiro.containerplay.web.projections.OSBuildConfigsProjection;

import io.fabric8.openshift.api.model.BuildConfig;

@RestController
@RequestMapping(value = "/v1/openshift/bc")
public class BuildConfigsController {

	@Autowired
	private BuildConfigsService buildConfigsService;
	@Autowired
	private ProjectionFactory projectionFactory;

	@GetMapping("/all")
	public List<OSBuildConfigsProjection> getAll(@RequestParam(name = "namespace", required=false) String namespace) {
		return buildConfigsService.getAll(namespace).stream()
				.map(buildConfig -> projectionFactory.createProjection(OSBuildConfigsProjection.class, buildConfig))
				.collect(Collectors.toList());
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public OSBuildConfigsProjection createOrUpdate(@RequestBody @Valid BuildConfig request, BindingResult result) {
		if (result.hasErrors())
			throw new ValidationException("OpenShift BuildConfig Add#", result.getFieldErrors());
		return projectionFactory.createProjection(OSBuildConfigsProjection.class, buildConfigsService.create(request));
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public boolean delete(@RequestBody @Valid BuildConfig request, BindingResult result) {
		if (result.hasErrors())
			throw new ValidationException("OpenShift BuildConfig delete#", result.getFieldErrors());
		return  buildConfigsService.delete(request);
	}
}
