package com.codemiro.containerplay.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.querydsl.core.types.Predicate;

/**
 * @author dvsingh9
 */

public interface BaseService<T, R> {
	Page<T> getAll(Predicate predicate, Pageable pageable);
	Page<T> getAll(String search, Pageable pageable);
	List<T> getAll(String search);
	T getById(Long id);
	T create(R request);
	void update(Long id, R request);
	void delete(Long id);
}
