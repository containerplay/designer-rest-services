package com.codemiro.containerplay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codemiro.containerplay.config.OpenShiftConfig;

import ch.qos.logback.classic.net.SyslogAppender;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.openshift.api.model.BuildConfig;
import io.fabric8.openshift.api.model.DeploymentConfig;
import io.fabric8.openshift.api.model.Project;
import io.fabric8.openshift.api.model.Route;
import io.fabric8.openshift.client.DefaultOpenShiftClient;
import io.fabric8.openshift.client.OpenShiftClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OpenShiftService {

	@Autowired
	private OpenShiftClient openShiftClient;
	

	public List<DeploymentConfig> listByNamespaceDeploymentConfigs(String namespace) {
		// OpenShiftClient oclient = kubernetesClient.adapt(OpenShiftClient.class);
		//OpenShiftClient client = new DefaultOpenShiftClient();
		return openShiftClient.deploymentConfigs().inNamespace(namespace).list().getItems();
	}

	public List<DeploymentConfig> listDeploymentConfigs() {
		// OpenShiftClient client = new DefaultOpenShiftClient();
		List<DeploymentConfig> list = openShiftClient.deploymentConfigs().list().getItems();
		return list;
	}


	public List<Secret> listSecrets() {
		return openShiftClient.secrets().list().getItems();
	}

	public List<io.fabric8.kubernetes.api.model.Service> listServices() {
		return openShiftClient.services().list().getItems();
	}

	public List<PersistentVolumeClaim> listPVCs() {
		return openShiftClient.persistentVolumeClaims().list().getItems();
	}

	public List<Route> listRoutes() {
		return openShiftClient.routes().list().getItems();
	}

	public List<Pod> listPods(String namespace) {
		return openShiftClient.pods().inNamespace(namespace).list().getItems();
	}

	public List<Project> listProjects() {
		return openShiftClient.projects().list().getItems();
	}

	
	public void ListAllProjectRelationShips() {
		// KubernetesClient kbClient = osConfig.getClusterConnection();
		List<Project> proj = openShiftClient.projects().list().getItems();
		proj.forEach(x ->  { 
			List<Pod>  pods = listPods(x.getMetadata().getName());
			System.out.println(pods);
			});
	}
	
	// namespace and deployment config cRUD

}
