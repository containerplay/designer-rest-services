package com.codemiro.containerplay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.NoRepositoryBean;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.StringPath;

/**
 * @author dvsingh9
 */

@NoRepositoryBean
public interface BaseRepository<T, QT>
		extends JpaRepository<T, Long>, QuerydslPredicateExecutor<T>, QuerydslBinderCustomizer<EntityPath<QT>> {
		
    default void customize(QuerydslBindings bindings, QT QType) {
       bindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
    }
}
