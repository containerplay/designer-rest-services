package com.codemiro.containerplay.cluster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.ReplicationControllerBuilder;

/**
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.fabric8.kubernetes.client.AutoAdaptableKubernetesClient;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;

public class OSServices {

    private static final Logger logger = LoggerFactory.getLogger(OSServices.class);

    public static void main(String[] args) throws InterruptedException {
        String master = "https://oc-cluster.anuragsdemo.club:8443/";
        if (args.length == 1) {
            master = args[0];
        }

        Config config = new ConfigBuilder().withMasterUrl(master)
          .withTrustCerts(true)
          .withUsername("developer")
          .withPassword("developer")
          .withNamespace("dev-env")
          .build();
        try (final KubernetesClient client = new AutoAdaptableKubernetesClient(config)) {

          log("Received pods", client.pods().list());
          
       // Create an RC
          
          ReplicationController rc = new ReplicationControllerBuilder()
                  .withNewMetadata().withName("nginx-controller").addToLabels("server", "nginx").endMetadata()
                  .withNewSpec().withReplicas(3)
                  .withNewTemplate()
                  .withNewMetadata().addToLabels("server", "nginx").endMetadata()
                  .withNewSpec()
                  .addNewContainer().withName("nginx").withImage("nginx")
                  .addNewPort().withContainerPort(80).endPort()
                  .endContainer()
                  .endSpec()
                  .endTemplate()
                  .endSpec().build();

          log("Created RC", client.replicationControllers().inNamespace("dev-env").create(rc));


        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);


            Throwable[] suppressed = e.getSuppressed();
            if (suppressed != null) {
                for (Throwable t : suppressed) {
                    logger.error(t.getMessage(), t);
                }
            }
        }
    }

    private static void log(String action, Object obj) {
        logger.info("{}: {}", action, obj);
    }

    private static void log(String action) {
        logger.info(action);
    }
}
