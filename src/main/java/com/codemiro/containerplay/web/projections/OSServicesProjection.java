package com.codemiro.containerplay.web.projections;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

public interface OSServicesProjection {

	 String getApiVersion();
	 
	 @Value("#{target.getMetadata() != null ? target.getMetadata().getName() : null}")
	 String getName1();
	 
	 String getKind();
	 
	 String getSpec();
}
