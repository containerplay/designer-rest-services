package com.codemiro.containerplay.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codemiro.containerplay.service.OpenShiftService;
import com.codemiro.containerplay.web.projections.OSBuildConfigsProjection;
import com.codemiro.containerplay.web.projections.OSDeploymentConfigsProjection;
import com.codemiro.containerplay.web.projections.OSPodProjection;
import com.codemiro.containerplay.web.projections.OSProjectProjection;
import com.codemiro.containerplay.web.projections.OSPvcProjection;
import com.codemiro.containerplay.web.projections.OSRouteProjection;
import com.codemiro.containerplay.web.projections.OSSecretProjection;
import com.codemiro.containerplay.web.projections.OSServicesProjection;

import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.openshift.api.model.BuildConfig;
import io.fabric8.openshift.api.model.DeploymentConfig;
import io.fabric8.openshift.api.model.Project;
import io.fabric8.openshift.api.model.Route;

@RestController
@RequestMapping(value = "/v1/openshift")
public class OpenShiftController {
	
	@Autowired private  OpenShiftService openShiftService;
	@Autowired private  ProjectionFactory projectionFactory;

	// get all configs Param (namespace)

	@GetMapping("/dc")
	public List<OSDeploymentConfigsProjection> searchAllDc(@RequestParam("namespace") String namespace) {
		List<DeploymentConfig> list = openShiftService.listByNamespaceDeploymentConfigs(namespace); // get list of DeploymentConfig from service
		List<OSDeploymentConfigsProjection> newList = list.stream()
				.map(deploymentConfig -> projectionFactory.createProjection(OSDeploymentConfigsProjection.class, deploymentConfig))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/dc/all")
	public List<OSDeploymentConfigsProjection> getAllDc() {
		List<DeploymentConfig> list = openShiftService.listDeploymentConfigs(); // get list of DeploymentConfig from service
		List<OSDeploymentConfigsProjection> newList = list.stream()
				.map(deploymentConfig -> projectionFactory.createProjection(OSDeploymentConfigsProjection.class, deploymentConfig))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	
	
	@GetMapping("/secrets")
	public List<OSSecretProjection> getAllSecrets(String search) {
		List<Secret> list= openShiftService.listSecrets();
		List<OSSecretProjection> newList = list.stream()
				.map(sec -> projectionFactory.createProjection(OSSecretProjection.class, sec))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/pvcs")
	public List<OSPvcProjection> getAllPvcs(String search) {
		List<PersistentVolumeClaim> list= openShiftService.listPVCs();
		List<OSPvcProjection> newList = list.stream()
				.map(pvc -> projectionFactory.createProjection(OSPvcProjection.class, pvc))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/routes")
	public List<OSRouteProjection> getAllRoutes(String search) {
		List<Route> list= openShiftService.listRoutes();
		List<OSRouteProjection> newList = list.stream()
				.map(route -> projectionFactory.createProjection(OSRouteProjection.class, route))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/pods")
	public List<OSPodProjection> getAllPods(String namespace) {
		List<Pod> list= openShiftService.listPods(namespace);
		List<OSPodProjection> newList = list.stream()
				.map(pod -> projectionFactory.createProjection(OSPodProjection.class, pod))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/services")
	public List<OSServicesProjection> getAllServices(String search) {
		List<Service> list= openShiftService.listServices();
		List<OSServicesProjection> newList = list.stream()
				.map(srv -> projectionFactory.createProjection(OSServicesProjection.class, srv))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/projects")
	public List<OSProjectProjection> getAllProjects(String search) {
		List<Project> list= openShiftService.listProjects();
		List<OSProjectProjection> newList = list.stream()
				.map(proj -> projectionFactory.createProjection(OSProjectProjection.class, proj))
				.collect(Collectors.toList()); // convert
		return newList;
	}
	
	@GetMapping("/rel")
	public void rel() {
		openShiftService.ListAllProjectRelationShips();
	}
	// /projectDetails
}
