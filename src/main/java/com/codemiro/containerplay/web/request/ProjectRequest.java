package com.codemiro.containerplay.web.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dvsingh9
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProjectRequest  implements Serializable {

	private static final long serialVersionUID = 442147852445845593L;

	private Long projectId;

	@NotNull
    @Size(max = 50)
	private String name;
	private String description;
	private String remarks;
	@NotNull
	private String projectHTML;
	@NotNull
	private String projectJsonConfigs;
	
	
}
